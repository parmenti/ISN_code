
class Joueur:

    def __init__(self, nom):
        self.pseudo       = nom
        self.top_scores   = []
        self.main         = []
        self.malus_manche = 0
        self.malus_cumule = 0


    def getPseudo(self):
        return self.pseudo
        
        
    def ajouter_carte(self, c):
        self.main.append(c)


    def choisir_carte(self, series):
        carte = None
        print('\nSeries en cours:\n')
        for s in series:
            print(str(s))
        print('\nMain:\n')
        self.afficher_main()
        indice = int(input('choix de la carte à jouer ? (1=première carte, etc.)\n'))
        carte  = self.main[indice-1]
        del self.main[indice-1]
        return carte


    def afficher_main(self):
        for c in range(len(self.main)):
            print('{:>7}'.format(str(self.main[c])),end='')
        print()
        for c in range(len(self.main)):
            print('{:>7}'.format(c+1),end='')
        print()
            

    def ajouter_malus(self, malus):
        self.malus_manche += malus


    def choisir_malus(self, series, c):
        print('\nSeries en cours:\n')
        for s in series:
            print(str(s))
        print('\nCarte jouée:\n')
        print(str(c))
        indice = int(input('\nChoix de la série à jouer ? (1=première série, etc.)\n'))
        return (indice-1)


    def maj_malus_cumule(self):
        self.malus_cumule += self.malus_manche
        self.malus_manche  = 0


    def getMalus_cumule(self):
        return self.malus_cumule


    def maj_top_scores(self):
        self.top_scores.sort()
        if len(self.top_scores) < 5 or len([x for x in self.top_scores if x > self.malus_cumule]) > 0:
            l = [x for x in self.top_scores if x < self.malus_cumule] + [self.malus_cumule] + [x for x in self.top_scores if x > self.malus_cumule]
            self.top_scores = l[:5]
