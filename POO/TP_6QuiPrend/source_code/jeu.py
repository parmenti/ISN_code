from partie import Partie 

def demarrer():
    noms = []
    continuer = True
    while(continuer):
        nom = input('Nom du joueur suivant ? (q pour quitter)\n')
        if nom != 'q':
            noms.append(nom)
        else:
            continuer = False
    p = Partie(noms)
    p.jouer_partie()
            

if __name__ == '__main__':
    demarrer()
