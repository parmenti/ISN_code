from random import randint
from serie  import Serie

class Manche:

    def __init__(self, ljoueurs, lc):
        self.joueurs = ljoueurs
        self.cartes  = lc
        self.series  = []
        self.tour    = 1


    def distribuer(self):
        for i in range(10):
            for j in self.joueurs:
                j.ajouter_carte(self.prendre_carte())

                
    def prendre_carte(self):
        indice = randint(0,len(self.cartes)-1)
        carte  = self.cartes[indice]
        del self.cartes[indice]
        return carte

    
    def creer_series(self):
        for i in range(4):
            c = self.prendre_carte()
            serie = Serie(c)
            self.series.append(serie)


    def trouver_serie(self, c):
        gaps = [None] * 4
        ming = -1 #indice du min
        for i in range(4):
            gaps[i] = self.series[i].difference(c)
            if gaps[i] > 0 and (ming == -1 or gaps[i] < gaps[ming]):
                ming = i
        return ming #diff positive minimale (si elle existe), -1 sinon


    def evaluer_malus(self, i, c, j):
        cumul = self.series[i].getMalus_serie()
        serie = Serie(c)
        j.ajouter_malus(cumul)
        self.series[i] = serie
        

    def placer_cartes(self, lc):
        lc.sort(key=lambda x: x[0].getValeur()) # liste triee par valeur de carte
        for (c,j) in lc:
            i = self.trouver_serie(c)
            if i != -1:
                self.series[i].ajouter_carte(c)
                if self.series[i].longueur() == 6:
                    self.evaluer_malus(i, c, j)
            else:
                ins = j.choisir_malus(self.series, c)
                self.evaluer_malus(ins, c, j) 


    def jouer(self):
        for i in range(10):
            lc = []
            for j in self.joueurs:
                #print(j.getPseudo() + ' joue')
                c = j.choisir_carte(self.series)                
                lc.append((c,j))
            self.placer_cartes(lc)
            for jo in self.joueurs:
                jo.maj_malus_cumule()
        
