
class Joueur:

    def __init__(self, nom, connexion):
        self.pseudo       = nom
        self.top_scores   = []
        self.main         = []
        self.malus_manche = 0
        self.malus_cumule = 0
        self.conn         = connexion


    def getPseudo(self):
        return self.pseudo
        
        
    def ajouter_carte(self, c):
        self.main.append(c)


    def choisir_carte(self, series):
        carte = None
        msg = '\n'
        for s in series:
             msg += str(s) + '\n'
        msg += self.afficher_main()
        msg += 'Numéro de carte choisie ?\n'
        self.conn.send(msg.encode("Utf8"))
        indice = int(self.conn.recv(1024).decode("Utf8"))
        carte  = self.main[indice-1]
        del self.main[indice-1]
        return carte


    def afficher_main(self):
        res = ''
        for c in range(len(self.main)):
            res += '{:>7}'.format(str(self.main[c]))
        res += '\n'
        for c in range(len(self.main)):
            res += '{:>7}'.format(c+1)
        res += '\n'
        return res
            

    def ajouter_malus(self, malus):
        self.malus_manche += malus


    def choisir_malus(self, series, c):
        msg = '\n'
        for s in series:
            msg += str(s) + '\n'
        msg += '\nCarte jouée:\n'
        msg += str(c)
        msg += '\nChoix de la série à jouer ? (1=première série, etc.)\n'
        self.conn.send(msg.encode("Utf8"))
        indice = int(self.conn.recv(1024).decode("Utf8"))
        return (indice-1)


    def maj_malus_cumule(self):
        self.malus_cumule += self.malus_manche
        self.malus_manche  = 0


    def getMalus_cumule(self):
        return self.malus_cumule


    def maj_top_scores(self):
        self.top_scores.sort()
        if len(self.top_scores) < 5 or len([x for x in self.top_scores if x > self.malus_cumule]) > 0:
            l = [x for x in self.top_scores if x < self.malus_cumule] + [self.malus_cumule] + [x for x in self.top_scores if x > self.malus_cumule]
            self.top_scores = l[:5]
