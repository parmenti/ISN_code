
class Carte:

    def __init__(self, val):
        self.valeur = val
        if val%10 == 0:
            self.malus = 3
        elif val%5 == 0:
            self.malus = 2
        elif val%11 == 0:
            self.malus = 5
        else:
            self.malus = 1
        if val == 55:
            self.malus = 7


    def getValeur(self):
        return self.valeur

    def getMalus(self):
        return self.malus

    def __str__(self):
        return '(' + str(self.valeur) + '/' + str(self.malus) + ')'
