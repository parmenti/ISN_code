import socket, sys

def demarrer_client(host, port): 
    # 1) création du socket :
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
 
    # 2) envoi d'une requête de connexion au serveur :
    try:
        mySocket.connect((host, port))
    except socket.error:
        print("La connexion a échoué.")
        sys.exit()

    # 3) réponse connexion
    msgServeur = mySocket.recv(1024).decode("Utf8")        
    print("S>", msgServeur)

    while True: # tant que le serveur n'indique pas la fin de partie
        msgServeur = mySocket.recv(1024).decode("Utf8")        
        if msgServeur =="FIN" or not msgServeur:
            break
        print("S>", msgServeur)
        msgClient = input("C> ")
        mySocket.send(msgClient.encode("Utf8"))
 
    # 4) fermeture de la connexion :
    print("Connexion interrompue.")
    mySocket.close()

if __name__ == '__main__': 
    demarrer_client('127.0.0.1', int(sys.argv[1]))
