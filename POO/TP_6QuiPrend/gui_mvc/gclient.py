from appJar import gui
import socket, sys, os

class client:

    def __init__(self, host=None, port=None):
        self.CARTE    = 0
        self.SERIE    = 1
        self.mode     = self.CARTE #mode par defaut
        self.tabs     = ['Series','Main','Score']
        self.nomJoueur= None
        self.mySocket = None
        self.gui      = gui("6 qui prend", "378x265")
        self.gui.setBg("orange")
        self.gui.setFont(18)
        self.gui.addLabel("title", "Jeu du 6 qui prend")
        self.gui.setLabelBg("title", "blue")
        self.gui.setLabelFg("title", "orange")
        self.gui.addLabelEntry("Hote")
        self.gui.addLabelEntry("Port")
        if host != None:
            self.gui.setEntry("Hote", host, callFunction=False)
        if port != None:
            self.gui.setEntry("Port", port, callFunction=False)
        self.gui.addButtons(["Jouer", "Annuler"], self.login)
        self.gui.setButtonSubmitFunction("Jouer", self.login)
        self.gui.setFocus("Hote")
        self.gui.go()

        
    def login(self, button):
        if button == "Annuler":
            self.gui.stop()
        else:
            hote = self.gui.getEntry("Hote")
            port = int(self.gui.getEntry("Port"))
            self.connect(hote,port)
            # Cacher la GUI de login (GUI principale)
            self.gui.hide()        
            # Lancer la partie
            self.jouer()

        
    def connect(self, host, port):
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.mySocket.connect((host, port))
            self.mySocket.setblocking(1)
        except socket.error:
            print("La connexion a échoué.",file=sys.stderr)
            sys.exit()
        msgServeur = self.mySocket.recv(1024).decode("Utf8")
        self.nomJoueur = msgServeur.split(':')[0]
        self.gui.infoBox("6 qui prend", msgServeur)

        
    def left(self):
        cur = self.gui.getTabbedFrameSelectedTab("Partie")
        pos =  next(i for i,x in enumerate(self.tabs) if x==cur)
        self.gui.setTabbedFrameSelectedTab("Partie", self.tabs[(pos-1)%3])


    def right(self):
        cur = self.gui.getTabbedFrameSelectedTab("Partie")
        pos =  next(i for i,x in enumerate(self.tabs) if x==cur)        
        self.gui.setTabbedFrameSelectedTab("Partie", self.tabs[(pos+1)%3])

        
    def valider(self, source):
        if self.mode == self.CARTE:
            selection = source[-2:]
            print(selection)
            self.mySocket.send(selection.encode("Utf8"))
            self.gui.destroySubWindow(self.nomJoueur)
            self.gui.after(500, self.jouer)


    def valider_serie(self, source):
        if self.mode == self.SERIE:
            selection = source[-2:]
            print(selection)
            self.mySocket.send(selection.encode("Utf8"))
            self.gui.destroySubWindow(self.nomJoueur)
            self.gui.after(500, self.jouer)
            

    def voir_main(self, series, lc, malus, cumul):
        ## Lancer la GUI de jeu (GUI secondaire)
        self.gui.startSubWindow(self.nomJoueur)
        self.gui.setSize(600,800)
        self.gui.setLocation(x=400*int(self.nomJoueur[-1]), y=None)
        self.gui.setFont(14)
        self.gui.setExpand("both")
        self.gui.setSticky("news")
        self.gui.bindKey('<Left>',  self.left)
        self.gui.bindKey('<Right>', self.right)
        self.gui.showSubWindow(self.nomJoueur)
        self.gui.startTabbedFrame("Partie")
        self.gui.setTabbedFrameTabExpand("Partie", expand=True)
        ## Contenu
        ### Series
        self.gui.startTab("Series")
        for s in range(4):
            ### Variables
            serie = series[s].split(',')
            choicenb  = 'serie' + '{:0>2}'.format(str(s+1))  
            ### Cartes cliquables
            self.gui.startLabelFrame(choicenb, 0, s%4)
            self.gui.setBg("yellow")
            for j in range(len(serie)):
                pixnumber = '{:0>3}'.format(str(serie[j]))
                self.gui.addImage("img" + pixnumber + choicenb, os.path.join("Cartes","Pico" + pixnumber + ".gif"))
                self.gui.zoomImage("img" + pixnumber + choicenb, -8)
                self.gui.setImageSubmitFunction("img" + pixnumber + choicenb, self.valider_serie)
            self.gui.stopLabelFrame()
        self.gui.stopTab()
        ### Main
        self.gui.startTab("Main")
        line = -1
        for i in range(len(lc)):
            if (i%5 == 0):
                line += 1
            ### Variables
            pixnumber = '{:0>3}'.format(str(lc[i]))
            choicenb  = '{:0>2}'.format(str(i+1))
            ### Cartes cliquables
            self.gui.startLabelFrame(choicenb, line, i%5)
            self.gui.setBg("lightgreen")            
            self.gui.setSticky("ew")
            self.gui.addImage("img" + choicenb, os.path.join("Cartes","Pico" + pixnumber + ".gif"))
            self.gui.zoomImage("img" + choicenb, -8)
            self.gui.setImageSubmitFunction("img" + choicenb, self.valider)
            self.gui.stopLabelFrame()
        self.gui.stopTab()
        ### Score
        self.gui.startTab("Score")
        self.gui.setBg('lightblue')
        self.gui.addLabel("Malus", "Malus " + self.nomJoueur + " : " + str(malus))
        self.gui.addLabel("Cumul", "Malus cumulé " + self.nomJoueur + " : " + str(cumul))
        self.gui.stopTab()
        self.gui.stopTabbedFrame()
            
        
    def jouer(self):
        ## Recuperer infos serveur
        msgServeur = self.mySocket.recv(1024).decode("Utf8")
        if msgServeur =="FIN" or not msgServeur:
            print("Connexion interrompue.")
            self.mySocket.close()
            sys.exit(1)
        ## Decoder message
        lc     = [] #liste des cartes composant la main du joueur
        series = [] #liste des series en cours
        malus  = 0
        cumul  = 0
        if msgServeur.startswith('poser:'):
            self.mode = self.CARTE
            series= msgServeur.split('\n')[0].split(':')[1].split(';')
            lc    = [int(x) for x in msgServeur.split('\n')[1].split(':')[1].split(',')]
            malus = msgServeur.split('\n')[2].split(':')[1]
            cumul = msgServeur.split('\n')[3].split(':')[1]
            #print(lc)
            #print(series)
            ## Afficher le jeu
            self.voir_main(series, lc, malus, cumul)                    
        elif msgServeur.startswith('remplacer:'):
            self.mode = self.SERIE
            series= msgServeur.split('\n')[0].split(':')[1].split(';')
            carte = msgServeur.split('\n')[1].split(':')[1]
            malus = msgServeur.split('\n')[2].split(':')[1]
            cumul = msgServeur.split('\n')[3].split(':')[1]            
            #print(series)
            #print(carte)
            self.gui.infoBox("Choix " + self.nomJoueur, 'Vous avez joue la carte ' + str(carte) + ', clickez sur la serie que vous voulez prendre')
            ## Afficher le jeu
            self.voir_main(series, lc, malus, cumul)
        elif msgServeur.startswith('vainqueur:'):
            winner = msgServeur.split(':')[1]
            self.gui.infoBox("Fin de la partie", "Le vainqueur est " + winner)


if __name__ == '__main__':
    host = None
    port = None
    if len(sys.argv) == 3:
        host = sys.argv[1]
        port = sys.argv[2]
    elif len(sys.argv) == 2:
        host = '127.0.0.1'
        port = sys.argv[1]
    sys.argv=['gclient.py'] # pour vider sys.argv avant que cette liste ne soit passee a la bibliotheque appJar
    client(host,port)
