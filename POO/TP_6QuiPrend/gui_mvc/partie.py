from joueur import Joueur
from carte  import Carte
from manche import Manche

class Partie:

    def __init__(self, lj, connexions):
        self.manches = []
        self.joueurs = []
        self.connex  = connexions
        for j in lj:
            joueur = Joueur(j, connexions[j]) # chaque joueur peut communiquer avec le jeu
            self.joueurs.append(joueur)


    def jouer_manche(self):
        cartes  = []
        for i in range(104):
            cartes.append(Carte(i+1))
        m = Manche(self.joueurs, cartes[:])
        m.distribuer()
        m.creer_series()
        m.jouer()
        self.manches.append(m)


    def jouer_partie(self):
        continuer = True
        while continuer:
            self.jouer_manche()
            malus = [x.getMalus_cumule() for x in self.joueurs if x.getMalus_cumule() >= 66]
            if len(malus) > 0:
                continuer = False
        minj   = 0
        for i in range(1,len(self.joueurs)):
            if self.joueurs[minj].getMalus_cumule() > self.joueurs[i].getMalus_cumule():
                minj = i
        winner = self.joueurs[minj]
        winner.maj_top_scores()
        msg    = 'vainqueur:' + winner.getPseudo()
        for c in self.connex:
            c.send(msg.encode("Utf8"))
        return winner

