from partie import Partie 
import socket, sys

def demarrer_serveur(host,port,nbjoueurs):
    noms = []
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    conn_client = {}	# dictionnaire des connexions clients    
    try:
        mySocket.bind((host, port))
    except socket.error:
        print("La liaison du socket à l'adresse choisie a échoué.")
        sys.exit()
    print("Serveur prêt, en attente de requêtes ...")
    mySocket.listen(5)
 
    # Attente et prise en charge des connexions demandées :
    while len(noms) < nbjoueurs:
        connexion, adresse = mySocket.accept()
        # Mémoriser la connexion dans le dictionnaire :
        igc = 'Joueur' + str(len(noms))
        noms.append(igc)
        conn_client[igc] = connexion
        print("Client %s connecté, adresse IP %s, port %s." %\
              (igc, adresse[0], adresse[1]))
        # Dialogue avec le client :
        msg =igc + ": vous êtes connecté.e"
        connexion.send(msg.encode("Utf8"))

    # Démarrage de la partie
    print('Clients connectés, la partie peut commencer')
    p = Partie(noms, conn_client)
    p.jouer_partie()


if __name__ == '__main__':
    demarrer_serveur('127.0.0.1', int(sys.argv[1]), 2)
