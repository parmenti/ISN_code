print('Number,Score')
for i in range(1,105):
    print(i,end=',')
    if i%10==0:
        print(3)
    elif i%5==0 and i!=55:
        print(2)
    elif i%11==0 and i!=55:
        print(5)
    elif i==55:
        print(7)
    else:
        print(1)
