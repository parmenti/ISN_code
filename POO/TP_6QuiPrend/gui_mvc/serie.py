
class Serie:

    def __init__(self, c):
        self.cartes      = [c]
        self.malus_serie = 0


    def __str__(self):
        res = ''
        for c in range(len(self.cartes)):
            res += str(self.cartes[c].getValeur())
            if c < len(self.cartes) - 1:
                res += ','
        res += ''
        return res


    def getMalus_serie(self):
        return self.malus_serie
    

    def longueur(self):
        return len(self.cartes)
    
    
    def ajouter_carte(self, c):
        self.cartes.append(c)
        self.malus_serie += c.getMalus()
    

    def difference(self, c):
        return c.getValeur() - self.cartes[-1].getValeur()
