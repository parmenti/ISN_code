# Définition d'un client réseau gérant en parallèle l'émission
# et la réception des messages (utilisation de 2 THREADS).
# Source : https://inforef.be/swi/download/apprendre_python3_5.pdf
import socket, sys
from threading import Thread

class ThreadReception(Thread):
    """objet thread gérant la réception des messages"""
    def __init__(self, conn, emetteur):
        Thread.__init__(self)
        self.connexion = conn # réf. du socket de connexion
        self.emetteur  = emetteur # thread emetteur

    def run(self):
        while True:
            message_recu = self.connexion.recv(1024).decode("Utf8")
            print("*" + message_recu + "*")
            if not message_recu or message_recu.upper() =="FIN":
                break
        # Le thread <réception> se termine ici.
        # On force la fermeture du thread <émission> :
        self.emetteur._stop()
        print("Client arrêté. Connexion interrompue.")
        self.connexion.close()
        
        
class ThreadEmission(Thread):
    """objet thread gérant l'émission des messages"""
    def __init__(self, conn):
        Thread.__init__(self)
        self.connexion = conn # réf. du socket de connexion

    def run(self):
        while True:
            message_emis = input()
            self.connexion.send(message_emis.encode("Utf8"))

class client:

    def __init__(self, host, port):
        self.connexion = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.connexion.connect((host, port))
        except socket.error:
            print("La connexion a échoué.")
            sys.exit(1)
        print("Connexion établie avec le serveur.")
        # Dialogue avec le serveur : on lance deux threads pour gérer
        # indépendamment l'émission et la réception des messages :
        self.emetteur = ThreadEmission(self.connexion)
        self.recepteur = ThreadReception(self.connexion, self.emetteur)
        self.emetteur.start()
        self.recepteur.start()


if __name__ == '__main__':
    if len(sys.argv) == 3:
        client(sys.argv[1], int(sys.argv[2]))
    elif len(sys.argv) == 2:
        client('127.0.0.1', int(sys.argv[1]))
    else:
        client('127.0.0.1', 45000)
