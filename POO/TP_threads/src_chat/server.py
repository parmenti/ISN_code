# Définition d'un serveur réseau gérant en parallèle plusieurs clients
# (utilisation de THREADS).
# Source : https://inforef.be/swi/download/apprendre_python3_5.pdf
import socket, sys
from threading import Thread

class ThreadClient(Thread):
    '''dérivation d'un objet thread pour gérer la connexion avec un client'''
    def __init__(self, conn, conn_client):
        Thread.__init__(self)
        self.connexion  = conn
        self.conn_client= conn_client

    def run(self):
        # Dialogue avec le client :
        nom = self.getName()
        # Chaque thread possède un nom
        while True:
            msgClient = self.connexion.recv(1024).decode("Utf8")
            if not msgClient or msgClient.upper() =="FIN":
                break
            message = "%s> %s" % (nom, msgClient)
            print(message)
            # Faire suivre le message à tous les autres clients :
            for cle in self.conn_client:
                if cle != nom:
                    # ne pas le renvoyer à l'émetteur
                    self.conn_client[cle].send(message.encode("Utf8"))            
        # Fermeture de la connexion :
        self.connexion.close()
        # couper la connexion côté serveur
        del conn_client[nom]
        # supprimer son entrée dans le dictionnaire
        print("Client %s déconnecté." % nom)
        # Le thread se termine ici


class server:

    def __init__(self, host, port):
        # Initialisation du serveur - Mise en place du socket :
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            mySocket.bind((host, port))
        except socket.error:
            print("La liaison du socket à l'adresse choisie a échoué.")
            sys.exit()
        print("Serveur prêt, en attente de requêtes ...")
        mySocket.listen(5)
        # Attente et prise en charge des connexions demandées par les clients :
        conn_client = {}
        # dictionnaire des connexions clients
        while True:
            connexion, adresse = mySocket.accept()
            # Créer un nouvel objet thread pour gérer la connexion :
            th = ThreadClient(connexion, conn_client)
            th.start()
            # Mémoriser la connexion dans le dictionnaire :
            it = th.getName()
            # identifiant du thread
            conn_client[it] = connexion
            print("Client %s connecté, adresse IP %s, port %s." %\
                (it, adresse[0], adresse[1]))
            # Dialogue avec le client :
            msg ="Vous êtes connecté. Envoyez vos messages."
            connexion.send(msg.encode("Utf8"))

if __name__ == '__main__':
    if len(sys.argv) == 3:
        server(sys.argv[1], int(sys.argv[2]))
    elif len(sys.argv) == 2:
        server('127.0.0.1', int(sys.argv[1]))
    else:
        server('127.0.0.1', 45000)
