{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Threads\n",
    "\n",
    "## Processus\n",
    "\n",
    "Lorsqu'un programme (écrit en python ou autre) est _exécuté_, le système d'exploitation créé un **processus** (ce qui implique entre autre le chargement des instructions du programme en mémoire et l'allocation de ressources telles que canaux de communication et adresses mémoires réservées). Même sur des architectures matérielles ne comportant qu'une seule unité de calcul (architecture mono-processeur), il est possible d'exécuter plusieurs programmes. C'est le système d'exploitation qui gère l'accès à l'unité de calcul (processeur), généralement au moyen d'une file de priorités. Ainsi, si plusieurs processus cohabitent en mémoire, ils peuvent être dans différents états (en fonction notamment de leur priorité) : en attente d'activation, en cours d'exécution, en pause (en attente de réactivation). Le cycle de vie d'un processus (i.e. ses différents états) sont représentés dans le diagramme ci-dessous :\n",
    "\n",
    "<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Diagramme_etat_processus_simple.svg/1280px-Diagramme_etat_processus_simple.svg.png\" alt=\"processus\" width=\"350x\" />\n",
    "\n",
    "Notons également (i) qu'il est possible d'exécuter plusieurs fois un _même_ programme, chaque instance du programme en mémoire correspond alors à un processus ayant un identifiant unique; et (ii) que les processus sont créés à partir d'autres processus (programmes en cours d'exécution), ils forment donc un arbre (le processus racine étant le processus lancé à l'ouverture de session).\n",
    "\n",
    "Dans un terminal sous linux, il est possible de lister les processus actifs au moyen des commandes `ps` et `top`, et de visualiser l'arbre des processus au moyen de la commande `pstree`.\n",
    "\n",
    "## Threads\n",
    "\n",
    "Un Thread est un processus qualifié de _léger_, car il ne dispose pas des mêmes ressources qu'un processus (notamment il ne dispose pas de mémoire dédiée, il utilise celle alloué à son processus parent).\n",
    "\n",
    "Les threads sont utilisés principalement dans 3 contextes :\n",
    " 1. pour les **calculs distribués**. Certains problèmes peuvent être décomposés en sous-problèmes indépendants qui peuvent être résolus en _parallèle_, pour peu que l'architecture matérielle le permette (cas des architectures multi-processeurs).\n",
    " 2. pour les **interfaces graphiques**. Par définition, ces interfaces ont besoin de réagir aux _événements_ provoqués par l'utilisateur de manière _concurrente_ (l'utilisateur peut vouloir lancer plusieurs actions sans attendre que chacune d'elle soit terminée.\n",
    " 3. pour la **communication asynchrone**. Lorsque plusieurs programmes communiquent via le réseau, il est souvant souhaitable de permettre à chaque programme de continuer à effectuer des traitements indépendemment de l'arrivée de messages.\n",
    "\n",
    "Un thread permet donc de _détacher_ certaines actions au cours de l'exécution d'un programme. Notons cependant que ce détachement peut parfois être temporaire : on parle alors de phases de _synchronisation_ (cas par exemple de plusieurs programme se partageant une ressource : plusieurs programmes écrivant dans le même fichier). Pour ces phases, on utilise un **verrou** (_lock_) appelé encore _jeton_.\n",
    "\n",
    "## En pratique\n",
    "\n",
    "Pour utiliser des `threads` en python, il faut hériter de la classe `Thread` définie dans le module `threading`. Chaque thread doit être _initialisé_ (cf constructeur en POO) et peut disposer d'une méthode _run_ pour passer le thread en état d'exécution :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "processus léger créé\n",
      "processus léger démarré\n"
     ]
    }
   ],
   "source": [
    "from threading import Thread\n",
    "\n",
    "class procleger(Thread):\n",
    "    def __init__(self):\n",
    "        Thread.__init__(self) # on appelle init de la classe parente\n",
    "        print('processus léger créé')\n",
    "        \n",
    "    def run(self):\n",
    "        print('processus léger démarré')\n",
    "        \n",
    "if __name__=='__main__':\n",
    "    p = procleger()\n",
    "    p.start() # start appelle la méthode run"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Comprendre les threads\n",
    "\n",
    "A présent, nous allons considérer le premier programme _séquentiel_ suivant permettant de répéter un mot 3 fois avec un délai d'attente aléatoire entre chaque lettre du mot (ici on utilise des instructions d'affichage de bas niveau plutôt que print pour contrôler chaque étape d'affichage) :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "threadthreadthread"
     ]
    }
   ],
   "source": [
    "import random, sys, time\n",
    "\n",
    "i = 0\n",
    "while i < 3:\n",
    "    for lettre in 'thread':\n",
    "        sys.stdout.write(lettre)\n",
    "        sys.stdout.flush()\n",
    "        attente = 0.2 + random.randint(1, 60) / 100\n",
    "        time.sleep(attente)\n",
    "    i += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Encapsulons ce programme d'affichage dans un thread correspondant à la classe `Afficheur` qui va être utilisée par la suite :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random, sys, time\n",
    "from threading import Thread\n",
    "\n",
    "class Afficheur(Thread):\n",
    "\n",
    "    \"\"\"Thread chargé simplement d'afficher un mot dans la console.\"\"\"\n",
    "    def __init__(self, mot):\n",
    "        Thread.__init__(self)\n",
    "        self.mot = mot\n",
    "\n",
    "    def run(self):\n",
    "        i = 0\n",
    "        while i < 3:\n",
    "            for lettre in self.mot:\n",
    "                sys.stdout.write(lettre)\n",
    "                sys.stdout.flush()\n",
    "                attente = 0.2 + random.randint(1, 60) / 100\n",
    "                time.sleep(attente)\n",
    "            i += 1\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous allons exécuteur deux afficheurs de manière concurrente, un afficheur de \"toto\" et un afficheur de \"titi\". Nous constaterons que l'ordre d'affichage des lettres des mots est pseudo-aléatoire (pas toujours le même à chaque exécution)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ttitoitottitoittoitottoi"
     ]
    }
   ],
   "source": [
    "if __name__ == \"__main__\":\n",
    "    a0 = Afficheur(\"toto\")\n",
    "    a1 = Afficheur(\"titi\")\n",
    "    a0.start()\n",
    "    a1.start()\n",
    "    a0.join()\n",
    "    a1.join()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notons que la méthode `join` permet au programme appelant (ici le programme python principal) d'attendre la fin du thread en question. Nous attendons donc que les deux threads soient terminés avant de terminer le programme principal (et de rendre la main au terminal par exemple).\n",
    "\n",
    "## Synchronisation\n",
    "\n",
    "Pour permettre à des threads de se synchroniser (par exemple ici de ne pas tronquer un mot en cours d'affichage), nous allons utiliser des verrous (lock) :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tototititototititototiti"
     ]
    }
   ],
   "source": [
    "import random, sys, time\n",
    "from threading import Thread, RLock\n",
    "\n",
    "verrou = RLock() # verrou commun (variable à portée globale)\n",
    "\n",
    "class Afficheur(Thread):\n",
    "\n",
    "    \"\"\"Thread chargé simplement d'afficher un mot dans la console.\"\"\"\n",
    "    def __init__(self, mot):\n",
    "        Thread.__init__(self)\n",
    "        self.mot = mot\n",
    "\n",
    "    def run(self):\n",
    "        i = 0\n",
    "        while i < 3:\n",
    "            time.sleep(0.8)\n",
    "            verrou.acquire()\n",
    "            for lettre in self.mot:\n",
    "                sys.stdout.write(lettre)\n",
    "                sys.stdout.flush()\n",
    "                attente = 0.2 + random.randint(1, 60) / 100\n",
    "                time.sleep(attente)\n",
    "            verrou.release()\n",
    "            i += 1\n",
    "                \n",
    "\n",
    "if __name__=='__main__':\n",
    "    a0 = Afficheur(\"toto\")\n",
    "    a1 = Afficheur(\"titi\")\n",
    "    a0.start()\n",
    "    a1.start()\n",
    "    a0.join()\n",
    "    a1.join()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Références:\n",
    "[OpenClassRoom](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/2235545-la-programmation-parallele-avec-threading)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
